<?php defined( 'ABSPATH' ) or die( 'Restricted access' );

class gPersianDateBBPress extends gPersianDateModuleCore
{

	protected $ajax = TRUE;

	protected function setup_actions()
	{
		add_filter( 'bbp_number_format', [ 'gPersianDateTranslate', 'numbers' ], 12 );
		add_filter( 'bbp_get_time_since', [ 'gPersianDateTranslate', 'numbers' ], 12 );
	}
}
